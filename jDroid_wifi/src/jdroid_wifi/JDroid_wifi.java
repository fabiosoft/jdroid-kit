/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jdroid_wifi;

import java.awt.Image;
import java.awt.Toolkit;

/**
 *
 * @author Fabio
 */
public class JDroid_wifi {
    
    public static final String SUBNET = "192.168.1";

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        MainForm form = new MainForm();
        // TODO imposta l'icona del programma  - non funziona
        Image im = Toolkit.getDefaultToolkit().getImage("stop.png");
        form.setIconImage(im);
        //---

        //imposta il form al centro dello schermo
        form.setSize(900, 380);
        form.pack();
        form.setLocationRelativeTo(null);
        //---

        //rende il form visibile  e focusable
        form.setVisible(true);
        form.setFocusable(true);
        
        //aggiorna gli host raggiungibili        
        form.checkHosts();
        
    }
}
