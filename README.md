![jdroidkit](https://bytebucket.org/fabiosoft/jdroid-kit/raw/2930edfa3ab2027f9e8cf4c74450d8d23b602d2a/relazioni/jdroid_arm.jpg =200x)

GO DOWN FOR ENGLISH
# JDroid KIT
## Braccio robotico + mouse controller

JDroid è un sistema che collega differenti moduli necessari per il trattamento degli elementi dal suo arrivo alla convalida dei cicli di lavoro.

Controlla la cartella "relazioni" tutte le ulteriori informazioni, news e aggiornamenti sulle funzioni.

### FreeStyle
* Controlla JDroid, con il tuo **mouse**!
* Movimenti **fluidi** dal tuo braccio, a quello robotico selezionando **l’articolazione** desiderata.
* Funzione **COMBO**! Più articolazioni e **gripper** funzionano in contemporanea!

### Ci penso io

* Imposta i cicli di lavoro, al resto pensa JDroid. Alla fine dell’esecuzione un beep segnalerà il successo!
* Con JDroid possiamo elaborare tutti i nostri oggetti con un unico, automatico e consolidato sistema tecnologico.

### PRO
* **Evoluzione**: Un vero sistema personalizzato ed evolutivo.
* **Flessibilità**: Idoneo per la configurazione e la flessibilità del vostro obiettivo.
* **Libertà**: Possibilità di completare azioni in modalità diverse.

Software/Hardware realizzato da Fabio Nisci e Diego Caridei.

----
ENGLISH: 
# JDroid KIT
## Robot arm + mouse controller


JDroid is a system that connects different modules necessary for the treatment of the elements from arrival to the validation of the work cycles.

Check the folder "relazioni" all further information, news and updates on functions.


### FreeStyle
* Control JDroid, with your **mouse**!
* **Fluid** movements from your arm to the robotic one choosing the desired **articulation**.
* **COMBO** feature! Many joints and **gripper** work simultaneously!

### Let me do this

* Set the work cycles, the rest let JDroid think. At the end of the execution a beep indicates the success!
* With JDroid you can process all of our items with a single, consolidated and automated technological system.

### PRO
* **Evolution**: A true custom system and evolutionary.
* **Flexibility**: Suitable for configuration and flexibility of your goal.
* **Freedom**: Ability to complete actions in different ways.

---
Software/Hardware developed by Fabio Nisci and Diego Caridei.